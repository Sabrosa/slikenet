#
# This file was taken from RakNet 4.082.
# Please see licenses/RakNet license.txt for the underlying license and related copyright.
#
#
# Modified work: Copyright (c) 2016-2019, SLikeSoft UG (haftungsbeschränkt)
#
# This source code was modified by SLikeSoft. Modifications are licensed under the MIT-style
# license found in the license.txt file in the root directory of this source tree.
#

add_library(SLikeNetLibStatic STATIC ${ALL_CPP_SRCS} ${CRYPTO_CPP_SRCS} ${ALL_HEADER_SRCS} ${CRYPTO_HEADER_SRCS})

if(NOT CMAKE_BUILD_TYPE STREQUAL "Debug")
	# by default we build the retail configuration
	add_definitions(-D_RETAIL -DNDEBUG)
	if(MSVC)
		set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} /O2 /Oi /Ot /GL")
		add_link_options(/LTGC /OPT:REF /OPT:ICF)
	else()
		set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -O2 -flto=auto")
	endif()
endif()

if(WIN32)
	add_definitions(-D_CRT_NONSTDC_NO_DEPRECATE -D_CRT_SECURE_NO_DEPRECATE)
else()
	set_target_properties(SLikeNetLibStatic PROPERTIES
		OUTPUT_NAME "slikenet"
	)
endif()

target_link_libraries(SLikeNetLibStatic ${SLIKENET_LIBRARY_LIBS})
if(WIN32)
	if(MSVC)
		set_target_properties(SLikeNetLibStatic PROPERTIES STATIC_LIBRARY_FLAGS "/NODEFAULTLIB:\"LIBCD.lib LIBCMTD.lib MSVCRT.lib\"")
	endif()
else()
	configure_file(${SLikeNet_SOURCE_DIR}/slikenet-config-version.cmake.in ${CMAKE_CURRENT_BINARY_DIR}/slikenet-config-version.cmake @ONLY)

	install(TARGETS SLikeNetLibStatic EXPORT SLikeNetLibStatic DESTINATION lib/slikenet)
	install(FILES ${ALL_HEADER_SRCS} DESTINATION include/slikenet)
	install(FILES ${CRYPTO_HEADER_SRCS} DESTINATION include/slikenet/crypto)
	install(FILES ../../slikenet-config.cmake ${CMAKE_CURRENT_BINARY_DIR}/slikenet-config-version.cmake DESTINATION lib/slikenet)
	install(EXPORT SLikeNetLibStatic FILE slikenet.cmake DESTINATION lib/slikenet)
endif()
